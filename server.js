const restify = require('restify');
const config = require('./src/config/config');
const log = require('./src/services/logger');
const path = require('path');


//
// Log application errors
//
process.on("error", () => {
    log.error(arguments);
});


const server = restify.createServer({
    name: 'Url shortener'
});

//
// Serve the static files
//
let wwwPath = path.join(__dirname, 'www');
log.info('Serving local path: ', wwwPath);

server.get('/*', restify.plugins.serveStatic({
    directory: wwwPath,
    default: 'index.html',
    appendRequestPath: false
}));



//
// Error Handlers
//
server.on('uncaughtException', (req, res, route, err) => {
    log.error(err.message, {
        event: 'uncaughtException'
    });

    res.send(500, {
        handler: err
    });
});

server.listen(config.port, () => {
    log.info(`${server.name} listening at ${server.url}`);
});