# URL Shortener
Note. This Web App is an extra application written to consume the URL Shortener API Service.

Online service that shortens a URL so that it can be pasted to other sites that has limited text length. The service will redirect the users browsers to the original URL upon clicking the short URL.

An Web App written in React JS.

###Features

- Does not store duplicate URLs. The same URL will give the same short URL everytime.
- Starts with short hash value (Base62), and expands as needed. This will give the shortest URL possible.
- Input has URL validation. Malicous javascript code is not possible.
- Designed to be less chatty to data store.
- URL length is limited to 2K
- It does not expire the store URLs
- There's no chance of generated short URL collisions during concurrency.

###Limitations that can be improved
- URLs are matched by their text values. It doesn't recognised URLs to be the same if one is encoded versus the other.
- It does not verify the existence of resource of the given URL
- It does not cache the URL translations in to the memory for faster
- The next new short URL value is predictable.
- The domain name is a lot longer than it should be for exercise purposes. In real-world, we need to buy a shorter domain name.

##How to use
Navigate to:   [https://url123-web.azurewebsites.net/](https://url123-web.azurewebsites.net/ "https://url123-web.azurewebsites.net/")

###Deployed
This Web App is continuously deployed to Azure App Service:  [https://url123-web.azurewebsites.net/](https://url123-web.azurewebsites.net/ "https://url123-web.azurewebsites.net/")

It consumes a URL Shortener API Service we wrote at: [https://url12.azurewebsites.net/](https://url12.azurewebsites.net/ "https://url12.azurewebsites.net/")

###How Edit
1. Clone this repo
1. Run `npm install`
1. Make changes
1. Run it locally:  `npm start`
1. Run tests: `npm run test`
1. Commit and push
