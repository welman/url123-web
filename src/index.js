import React from 'react';
import ReactDOM from 'react-dom';
import UrlShortener from './Components/UrlShortener/index';

ReactDOM.render(
    <UrlShortener />,
    document.getElementById('app')
);