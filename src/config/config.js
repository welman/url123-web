
let localConfig = null;
try {
    localConfig = require('./config.local'); 
} catch (ex) {
    // ignore, prod doesn't have local config
}

module.exports = Object.assign({
    urlService: process.env.URL_SERVICE,
    port: process.env.PORT,

}, localConfig);




