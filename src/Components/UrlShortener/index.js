import React from 'react';
import {shortenUrl} from './services';
import log from '../../services/logger';

class UrlShortener extends React.Component {
    constructor(props) {
      super(props);
      this.performUrlShortening = this.performUrlShortening.bind(this);
      this.shortenUrlDone = this.shortenUrlDone.bind(this);

      this.state = {
          longUrl: null,
          shortUrl: null,
          message: null
        };
    }
  
    performUrlShortening(longUrl) {
        this.setState({
            shortUrl: null,
            message: null
        });
        
        log.info('Performing url shortening for url ', longUrl);

        shortenUrl(longUrl)
            .then(data => this.shortenUrlDone(data))
            .catch(reason => {
                log.error('There was unexpected error shortening the url from API service.', reason);
                this.setState({message: String(reason)});
            });
    }

    shortenUrlDone(data) {
        log.info('The url is shortened to ', data);

        this.setState({shortUrl: data.shortUrl});
    }

    render() {
        return (
            <div>
                <h2>URl Shortener</h2>
                <section>
                    <label val>URL to Shorten:</label>
                    <input type="text" maxLength="2000" onChange={(e) => this.setState({longUrl: e.target.value})} />
                    <button onClick={() => this.performUrlShortening(this.state.longUrl)}>Shorten</button>
                </section>
                <br/>
                <section>
                    <label>Short URL:</label>
                    <div className="green">{this.state.shortUrl}</div>
                    <div className="red">{this.state.message}</div>
                </section>
            </div>
        );
    }
}

export default UrlShortener;