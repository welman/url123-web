import fetch from 'isomorphic-fetch';
import config from '../../config/config.js';
import log from '../../services/logger';

export function shortenUrl(longUrl) {

    return fetch(config.urlService,
            {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                credentials: 'same-origin',
                body: JSON.stringify({
                    longUrl
                }),
            })
        .then( response => {
            //To make failed status falls to catch, because by default fetch does not
            if (response.status >= 400) {
                log.error('API response: ', response);

                //Return a Rejected Promise
                return response.json().then( json => {
                    log.error('API response json: ', json);
                    throw new Error(json && json.message
                        ? json.message 
                        : 'Something went wrong');
                });
            }
            else
                return response;
        })
        .then( response => response.json() );
}
